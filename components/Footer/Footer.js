import Link from 'next/link'
import style from './Footer.less'
import { Icon } from 'antd'

const Header = () => {
    return (
        <div className="footer">
            {/*<Link href="/"><a style={linkStyle}>홈</a></Link>*/}
            {/*<Link href="/about"><a style={linkStyle}>소개</a></Link>*/}
            {/*<Link href="/ssr-test"><a style={linkStyle}>SSR 테스트</a></Link>*/}
            <Link href="/">
                <div className="footer-box">
                    <div className="footer-logo">
                        <a className="footer-text">Muscial Chair Footer</a>
                    </div>
                </div>
            </Link>

        </div>
    );
};

export default Header;
