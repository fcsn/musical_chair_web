import Link from 'next/link'
import style from './Header.less'
import { Icon } from 'antd'

const Header = () => {
    return (
        <div className="header">
            <div className="navigation-box">
                <Icon className="navigation-icon" type="bars"/>
            </div>
            <Link href="/">
                <div className="logo-box">
                    <div className="logo">
                        <a className="logo-text">Muscial Chair</a>
                    </div>
                </div>
            </Link>
            <div className="search-box">
                <Icon className="search-icon" type="search"/>
            </div>
        </div>
    );
};

export default Header;
