import Header from '../Header';
import Footer from '../Footer';
import style from './Layout.less';

const Layout = ({children}) => (
    <section className="container">
        <div className="main-box">
            <Header/>
            <div className="main-content">
            {children}
            </div>
            <Footer/>
        </div>
    </section>
);

export default Layout;
