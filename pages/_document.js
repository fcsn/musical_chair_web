import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'

export default class MyDocument extends Document {
    static getInitialProps ({ renderPage }) {
        const { html, head } = renderPage()
        const styles = flush()

        return { html, head, styles }
    }

    render () {
        return (
            <html>
                <Head>
                    <style>{`body { margin: 0 } /* custom! */`}</style>
                    {/*<title>Musical Chair</title>*/}
                </Head>

                <body style={{
                    margin: '0',
                    boxSizing: 'border-box',
                    fontFamily: 'sans-serif',
                    background: '#3b3b3c',
                    height: '100vh',
                }}>
                    <Main />
                    <NextScript />
                </body>
            </html>
        )
    }
}
