import Link from 'next/link'
import Layout from '../components/Layout'
import Head from 'next/head'
import { connect } from 'react-redux'
import style from './index.less'
import { Icon } from 'antd'
import 'antd/dist/antd.css'

class Index extends React.Component {

    render () {
        return (
            <Layout>
                <Head>
                    <title>
                        Home - Musical Chair -
                    </title>
                </Head>
                <div className="main-banner">
                    <div className="main-img">
                        <Link href="/jobs/performance">
                            <button className="main-button">
                                <div className="button-text">Find Job <Icon type="smile" /></div>
                            </button>
                        </Link>
                    </div>
                </div>

                <div className="main-buttons-box">
                    <div className="main-buttons-1">
                        <Link href="/jobs/performance">
                            <button className="main-buttons-button-1">Performance Jobs</button>
                        </Link>
                        <Link href="/jobs/teaching">
                            <button className="main-buttons-button-1">Teaching Jobs</button>
                        </Link>
                    </div>
                    <div className="main-buttons-2">
                        <Link href="/about">
                            <button className="main-buttons-button-2">About Us</button>
                        </Link>
                    </div>
                </div>
            </Layout>
        )
    }
}

function mapStateToProps(state) {
    return {
        members: state.members
    }
}
export default connect(mapStateToProps)(Index)
