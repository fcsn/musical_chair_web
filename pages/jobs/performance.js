import Layout from '../../components/Layout'
import style from './performance.less'
import axios from 'axios'
import { Card, Carousel, Icon } from 'antd'
import 'antd/dist/antd.css'

class Performance extends React.Component {
    static async getInitialProps ({req}) {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users')
        return {
            users: response.data
        }
    }

    render () {
        return (
            <Layout>
                <div>
                    <Carousel effect="fade">
                        <div>
                            <h3 style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', height: '5.5rem', background: '#dee2e6', paddingTop: '1rem'}}>
                                <Icon type="question-circle" style={{color: '#868e96', fontSize: '3rem'}} />
                                <Icon type="question-circle" style={{color: '#a61e4d', fontSize: '3rem'}} />
                                <Icon type="question-circle" style={{color: '#868e96', fontSize: '3rem'}} />
                                <Icon type="question-circle" style={{color: '#868e96', fontSize: '3rem'}} />
                            </h3>
                        </div>
                        <div>
                            <h3 style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', height: '5.5rem', background: '#dee2e6', paddingTop: '1rem'}}>
                                <Icon type="question-circle" style={{color: '#868e96', fontSize: '3rem'}} />
                                <Icon type="question-circle" style={{color: '#868e96', fontSize: '3rem'}} />
                                <Icon type="question-circle" style={{color: '#868e96', fontSize: '3rem'}} />
                                <Icon type="question-circle" style={{color: '#868e96', fontSize: '3rem'}} />
                            </h3>
                        </div>
                    </Carousel>
                </div>

                <div className="performance-title">
                    Performance Jobs
                    <span className="perfomance-subtitle"> - latest posts</span>
                </div>

                <p className="latest-post">post1</p>
                <p className="latest-post">post1</p>
                <p className="latest-post">post1</p>
                <p className="latest-post">post1</p>
                <p className="latest-post">post1</p>

                {/*<div style={{display: 'flex', justifyContent: 'center'}}>*/}
                    {/*<Card*/}
                        {/*hoverable*/}
                        {/*style={{ width: "90%", marginBottom: '1rem', border: '1px solid #ced4da' }}*/}
                        {/*className="performance-card"*/}
                        {/*cover={<img alt="example" src="https://www.kesslerandsons.com/wp-content/uploads/2015/11/horn.jpg" />}*/}
                    {/*>*/}
                        {/*<div style={{textAlign: 'center', fontSize: '1rem'}}>Brass</div>*/}
                        {/*<p style={{fontSize: '1.1rem'}}>french horn (11)</p>*/}
                        {/*<p style={{fontSize: '1.1rem'}}>trumpet (9)</p>*/}
                        {/*<p style={{fontSize: '1.1rem'}}>trombone (12)</p>*/}
                        {/*<p style={{fontSize: '1.1rem'}}>tuba (4)</p>*/}
                        {/*<p style={{fontSize: '1.1rem'}}>euphonium (12)</p>*/}
                    {/*</Card>*/}
                {/*</div>*/}

                {/*<div style={{display: 'flex', justifyContent: 'center'}}>*/}
                    {/*<Card*/}
                        {/*hoverable*/}
                        {/*style={{ width: "90%", marginBottom: '1rem', border: '1px solid #ced4da' }}*/}
                        {/*cover={<img alt="example" height={230} src="https://www.kesslerandsons.com/wp-content/uploads/2015/10/howarth-s40c-logo.jpg" />}*/}
                    {/*>*/}
                        {/*<div style={{textAlign: 'center', fontSize: '1.2rem'}}>Woodwind</div>*/}
                    {/*</Card>*/}
                {/*</div>*/}
            </Layout>
        )
    }
}

export default Performance

